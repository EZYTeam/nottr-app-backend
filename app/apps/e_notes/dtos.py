from sys import getsizeof
from uuid import UUID

from pydantic import Field, validator

from app.core.config import get_settings
from app.core.pydantic_base import BaseModel, BaseModelORM
from app.lib.classes import BadRequestException

settings = get_settings()


class ENoteDTO(BaseModelORM):
    id: UUID | str | None = None
    enc_data: str

    @validator('enc_data')
    def check_size_is_less_than(cls, v: str) -> str:
        if getsizeof(v) > settings.MAX_E_NOTE_SIZE:
            raise BadRequestException(detail='Max e_note size is reached.')

        return v


class ENotesListDTO(BaseModel):
    e_notes: list[ENoteDTO] = Field(default_factory=list)
