import uuid
from typing import TYPE_CHECKING

from sqlalchemy import ForeignKey, String, Text
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.core.models import DBBaseModel

if TYPE_CHECKING:
    from app.apps.users.models import Users


class ENotes(DBBaseModel):
    __tablename__: str = 'e_notes'

    enc_data: Mapped[str] = mapped_column(Text(), nullable=False)

    user_id: Mapped[uuid.UUID] = mapped_column(ForeignKey('users.id'), index=True)
    user: Mapped['Users'] = relationship(back_populates='e_notes')
