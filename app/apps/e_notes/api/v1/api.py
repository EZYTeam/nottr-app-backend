from fastapi import APIRouter, Depends, status
from fastapi.responses import ORJSONResponse

from app.apps.e_notes.dtos import ENoteDTO, ENotesListDTO
from app.apps.e_notes.use_cases import (
    CreateENoteForUserUseCase,
    DeleteENoteForUserUseCase,
    FetchENotesForUserUseCase,
    PatchENoteForUserUseCase,
)
from app.apps.users.dependencies import get_current_user
from app.apps.users.models import Users
from app.lib.classes import MessageDTO

router = APIRouter(
    prefix='/e-notes',
    tags=['e-notes'],
    default_response_class=ORJSONResponse,
)


@router.get('/list', response_model=ENotesListDTO, status_code=status.HTTP_200_OK)
async def get_e_notes(
    user: Users = Depends(get_current_user),
) -> ENotesListDTO:
    return ENotesListDTO(e_notes=await FetchENotesForUserUseCase(user=user).execute())


@router.post('/create', response_model=ENoteDTO, status_code=status.HTTP_201_CREATED)
async def create_e_note(e_note: ENoteDTO, user: Users = Depends(get_current_user)) -> ENoteDTO:
    return ENoteDTO.from_orm(await CreateENoteForUserUseCase(e_note=e_note, user=user).execute())


@router.patch('/update', response_model='', status_code=status.HTTP_200_OK)
async def patch_e_note(
    e_note: ENoteDTO,
    user: Users = Depends(get_current_user),
) -> ENoteDTO:
    return ENoteDTO.from_orm(await PatchENoteForUserUseCase(e_note=e_note, user=user).execute())


@router.delete('/{e_note_id}/delete', response_model=MessageDTO, status_code=status.HTTP_200_OK)
async def delete_e_note(
    e_note_id: str,
    user: Users = Depends(get_current_user),
) -> MessageDTO:
    await DeleteENoteForUserUseCase(e_note_id=e_note_id, user=user).execute()
    return MessageDTO(message=f'Successfully deleted e_note {e_note_id}!')
