from abc import ABC
from uuid import UUID

from sqlalchemy import and_, select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import selectinload

from app.apps.e_notes.dtos import ENoteDTO
from app.apps.e_notes.models import ENotes
from app.apps.users.models import Users
from app.lib.classes import BadRequestException, SQLAlchemySessionBaseUseCase, alchemy_session_decorator


class BaseENoteWithUserUseCase(SQLAlchemySessionBaseUseCase, ABC):
    def __init__(self, e_note: ENoteDTO, user: Users) -> None:
        super().__init__()
        self.user: Users = user
        self.e_note: ENoteDTO = e_note

    async def _get_e_note_for_user_by_id(self, session: AsyncSession) -> ENotes:
        q = (
            select(ENotes)
            .where(and_(ENotes.user_id == self.user.id, ENotes.id == self.e_note.id))
            .limit(1)  # type: ignore[attr-defined]
        )
        res: None | ENotes = (await session.scalars(q)).first()
        if not res:
            raise BadRequestException(detail='ENote not found')

        return res


class FetchENotesForUserUseCase(SQLAlchemySessionBaseUseCase):
    def __init__(self, user: Users) -> None:
        super().__init__()
        self._user = user

    @alchemy_session_decorator
    async def execute(self, session: AsyncSession) -> list[ENoteDTO]:
        user = await session.get(
            Users,
            self._user.id,
            options=[selectinload(Users.e_notes)],
            populate_existing=True,
        )
        if not user:
            return []

        return [ENoteDTO.from_orm(e_note) for e_note in user.e_notes]


class CreateENoteForUserUseCase(BaseENoteWithUserUseCase):
    @alchemy_session_decorator
    async def execute(self, session: AsyncSession) -> ENotes:
        if self.e_note.id:
            raise BadRequestException(detail='Can\'t create already existing ENote')

        e_note = ENotes(
            user_id=self.user.id,
            **self.e_note.dict(exclude_none=True),
        )
        session.add(e_note)
        await session.flush()

        return e_note


class PatchENoteForUserUseCase(BaseENoteWithUserUseCase):
    @alchemy_session_decorator
    async def execute(self, session: AsyncSession) -> ENotes:
        if not self.e_note.id:
            raise BadRequestException(detail='No e_note_id provided')

        e_note = await self._get_e_note_for_user_by_id(session=session)

        if e_note.enc_data == self.e_note.enc_data:
            return e_note

        e_note.enc_data = self.e_note.enc_data
        await session.flush()

        return e_note


class DeleteENoteForUserUseCase(SQLAlchemySessionBaseUseCase):
    def __init__(self, e_note_id: str | UUID, user: Users) -> None:
        super().__init__()
        self.user: Users = user
        self.e_note_id: str | UUID = e_note_id

    @alchemy_session_decorator
    async def execute(self, session: AsyncSession) -> None:
        q = (
            select(ENotes)
            .where(and_(ENotes.user_id == self.user.id, ENotes.id == self.e_note_id))
            .limit(1)  # type: ignore[attr-defined]
        )
        e_note: None | ENotes = (await session.scalars(q)).first()
        if not e_note:
            raise BadRequestException(detail='ENote not found')

        await session.delete(e_note)
        await session.flush()

        return None
