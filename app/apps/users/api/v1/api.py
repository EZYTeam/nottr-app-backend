from fastapi import APIRouter, HTTPException
from fastapi.requests import Request
from fastapi.responses import ORJSONResponse, RedirectResponse
from starlette import status

from app.apps.users.dtos import ClientIDInfo, JWTToken
from app.apps.users.use_cases import AuthenticateUserUseCase
from app.core.config import get_settings
from app.lib.utils import oauth2_google

router = APIRouter(
    prefix='/users',
    tags=['users'],
    default_response_class=ORJSONResponse,
)
settings = get_settings()


@router.get('/google-login')
async def google_redirect(request: Request) -> RedirectResponse:
    redirect_uri = request.url_for('auth').replace('http://', 'https://')
    return await oauth2_google.google.authorize_redirect(request, redirect_uri)


@router.get('/google-auth', name='auth')
async def auth(request: Request) -> RedirectResponse:
    token = await oauth2_google.google.authorize_access_token(request)
    user = token.get('userinfo')
    if user:
        user_token: JWTToken = await AuthenticateUserUseCase(username=user['sub']).execute()
        return RedirectResponse(
            url=f'{settings.SELF_HOST}/login/{user_token.access_token}',
            status_code=status.HTTP_302_FOUND,
        )

    raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)


@router.get('/client-id')
async def get_client_id() -> ClientIDInfo:
    return ClientIDInfo(client_id=settings.CLIENT_ID)
