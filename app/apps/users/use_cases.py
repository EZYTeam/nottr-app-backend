from datetime import datetime, timedelta

from fastapi import HTTPException, status
from jose import ExpiredSignatureError, JWTError, jwt
from jose.exceptions import JWTClaimsError
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession, async_sessionmaker

from app.apps.users.dtos import JWTToken, JWTTokenData, RegistrationData
from app.apps.users.models import Users
from app.core.config import get_settings
from app.core.db_config import AsyncSessionMaker
from app.lib.classes import BadRequestException, SQLAlchemySessionBaseUseCase, alchemy_session_decorator

settings = get_settings()


class RegisterUserUseCase(SQLAlchemySessionBaseUseCase):
    """
    Registering new user
    """

    def __init__(self, registration_data: RegistrationData) -> None:
        super().__init__()
        self._registration_data = registration_data

    @alchemy_session_decorator
    async def execute(self, session: AsyncSession) -> Users:
        q = (
            select(Users)
            .where(Users.username == self._registration_data.username)
            .exists()  # type: ignore[attr-defined]
        )

        if v := (await session.scalars(q.select())).first():
            raise BadRequestException(
                detail=f'User already exists {v}',
            )

        user = Users(
            username=self._registration_data.username,
        )

        session.add(user)
        await session.flush()
        return user


class AuthenticateUserUseCase(SQLAlchemySessionBaseUseCase):
    """
    Authenticating a user via log/pass and generating an access token for him
    """

    def __init__(self, username: str) -> None:
        super().__init__()
        self._username = username.lower()

    @alchemy_session_decorator
    async def execute(self, session: AsyncSession) -> JWTToken:
        q = select(Users).where(Users.username == self._username)
        user: None | Users = (await session.scalars(q)).first()
        if not user:
            user = await RegisterUserUseCase(registration_data=RegistrationData(username=self._username)).execute()

        return JWTToken(
            access_token=await self._create_access_token(data={'sub': user.username}),  # type: ignore[union-attr]
            token_type='bearer',
        )

    @staticmethod
    async def _create_access_token(data: dict) -> str:
        to_encode = data.copy()
        to_encode.update({'exp': datetime.utcnow() + timedelta(minutes=settings.EXPIRATION_MINUTES)})
        return jwt.encode(to_encode, settings.SECRET_KEY.get_secret_value(), algorithm=settings.ALGORITHM)


class GetUserFromTokenUseCase(SQLAlchemySessionBaseUseCase):
    """
    Looking for user from token.
    """

    def __init__(self, token: str) -> None:
        super().__init__()
        self._token = token
        self._session_maker: async_sessionmaker[AsyncSession] = AsyncSessionMaker

    async def execute(self) -> Users:
        exception = HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, headers={'WWW-Authenticate': 'Bearer'})
        try:
            payload = jwt.decode(self._token, settings.SECRET_KEY.get_secret_value(), algorithms=[settings.ALGORITHM])
            username: str | None = payload.get('sub')
            if username is None:
                exception.detail = 'Token is corrupted or has expired.'
                raise exception
        except (JWTError, ExpiredSignatureError, JWTClaimsError):
            exception.detail = 'Token is corrupted or has expired.'
            raise exception

        token_data = JWTTokenData(username=username)
        return await self._get_user(token_data=token_data, exception=exception)

    @alchemy_session_decorator
    async def _get_user(self, session: AsyncSession, token_data: JWTTokenData, exception: HTTPException) -> Users:
        q = select(Users).where(Users.username == token_data.username)
        user: Users | None = (await session.scalars(q)).first()
        if user is None:
            exception.detail = 'Token is not valid.'
            raise exception

        return user
