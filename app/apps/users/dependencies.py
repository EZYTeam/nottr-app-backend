from http.client import HTTPException

from fastapi import Depends, status
from fastapi.requests import Request

from app.apps.users.models import Users
from app.apps.users.use_cases import GetUserFromTokenUseCase
from app.lib.utils import oauth2_internal_scheme


async def get_current_user(token: str = Depends(oauth2_internal_scheme)) -> Users:
    return await GetUserFromTokenUseCase(token=token).execute()
