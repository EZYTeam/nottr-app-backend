from pydantic import Field, SecretStr, validator

from app.core.pydantic_base import BaseModel, BaseModelORM


class JWTToken(BaseModel):
    access_token: str
    token_type: str


class JWTTokenData(BaseModel):
    username: str | None


class RegistrationData(BaseModel):
    username: str

    @validator('username')
    def to_lowercase(cls, v: str) -> str:
        return v.lower()


class UserDTO(BaseModelORM):
    username: str


class ClientIDInfo(BaseModel):
    client_id: str
