from typing import TYPE_CHECKING

from pydantic import SecretStr
from sqlalchemy import String
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.core.models import DBBaseModel
from app.lib.utils import pwd_context

if TYPE_CHECKING:
    from app.apps.e_notes.models import ENotes


class Users(DBBaseModel):
    __tablename__: str = 'users'

    username: Mapped[str] = mapped_column(String(length=128), nullable=False)

    e_notes: Mapped[list['ENotes']] = relationship(back_populates='user', cascade='all, delete')
