from decimal import Decimal
from functools import wraps
from typing import Any, Awaitable, Callable

import orjson
from authlib.integrations.starlette_client import OAuth
from fastapi.security import OAuth2PasswordBearer
from passlib.context import CryptContext

from app.core.config import get_settings

settings = get_settings()


def serialize_decimals(obj: Any) -> str:
    if isinstance(obj, Decimal):
        return str(obj)
    raise TypeError


def orjson_dumps(v: Any) -> str:  # noqa: WPS111
    """ORJSON dumps with decimals to str by default"""
    return orjson.dumps(v, default=serialize_decimals).decode()


def async_wrap(func: Callable) -> Callable:
    import asyncio
    from concurrent.futures import ThreadPoolExecutor

    pool = ThreadPoolExecutor()

    @wraps(func)
    def wrapper(*args: Any, **kwargs: Any) -> Awaitable:
        future = pool.submit(func, *args, **kwargs)
        return asyncio.wrap_future(future)  # make it awaitable

    return wrapper


pwd_context = CryptContext(
    schemes=['argon2'],
    deprecated=['auto'],
    argon2__type='ID',
    argon2__time_cost=8,
    argon2__memory_cost=256000,
    argon2__parallelism=4,
)
oauth2_internal_scheme = OAuth2PasswordBearer(tokenUrl='/api/v1/users/login')


oauth2_google = OAuth()
oauth2_google.register(
    name='google',
    client_id=settings.CLIENT_ID,
    client_secret=settings.CLIENT_SECRET.get_secret_value(),
    server_metadata_url='https://accounts.google.com/.well-known/openid-configuration',
    client_kwargs={'scope': 'openid'},
)
