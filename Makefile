IMAGE_PATH := ${CI_REGISTRY}/nottr-app-backend
DOCKERFILE_PATH := ./docker
DOCKERFILE := $(DOCKERFILE_PATH)/Dockerfile

VCS_REF = $(shell git rev-parse --short HEAD)
TAG_NAME ?= dev-${VCS_REF}
BUILD_DATE = $(shell date -u +"%Y-%m-%dT%H:%M:%SZ")

.login:
	${INFO} "Logging in to Registry..."
	@docker login --username ${CI_REGISTRY_USER} --password ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}
	${INFO} "Logged in"

.PHONY: build
build: .login
	${INFO} "Building app..."
	@DOCKER_BUILDKIT=1 docker build -f "${DOCKERFILE}" -t "${IMAGE_PATH}:${TAG_NAME}" --build-arg VCS_REF="${VCS_REF}" --build-arg BUILD_DATE="${BUILD_DATE}" .
	${INFO} "Built"
	${INFO} "Pushing app to docker registry..."
	@docker push "${IMAGE_PATH}:${TAG_NAME}"
	${INFO} "Pushed"

YELLOW := "\e[1;33m"
NC := "\e[0m"

INFO := @sh -c '\
    printf $(YELLOW); \
    echo "=> $$1"; \
    printf $(NC)' VALUE
