from uuid import uuid4

import pytest
from fastapi.testclient import TestClient
from pytest_mock import MockerFixture

from app.apps.e_notes.use_cases import (
    CreateENoteForUserUseCase,
    DeleteENoteForUserUseCase,
    FetchENotesForUserUseCase,
    PatchENoteForUserUseCase,
)


@pytest.mark.asyncio
async def test_get_e_notes_route(mocker: MockerFixture, client: TestClient, normal_user_token_headers):
    mock_e_notes = mocker.patch.object(
        FetchENotesForUserUseCase,
        'execute',
        return_value=[mocker.Mock(id=str(uuid4()), enc_data='test') for i in range(10)],
    )

    response = client.get('api/v1/e-notes/list', headers=normal_user_token_headers)

    assert response.status_code == 200
    assert mock_e_notes.call_count == 1


@pytest.mark.asyncio
async def test_create_e_note_route(mocker: MockerFixture, client: TestClient, normal_user_token_headers):
    mock_create = mocker.patch.object(
        CreateENoteForUserUseCase,
        'execute',
        return_value=mocker.Mock(
            id=str(uuid4()),
            enc_data='test_enc_data',
        ),
    )

    response = client.post(
        'api/v1/e-notes/create',
        json={'enc_data': 'test_enc_data'},
        headers=normal_user_token_headers,
    )

    assert response.status_code == 201
    assert mock_create.call_count == 1


@pytest.mark.asyncio
async def test_patch_e_note_route(mocker: MockerFixture, client: TestClient, normal_user_token_headers):
    mock_patch = mocker.patch.object(
        PatchENoteForUserUseCase,
        'execute',
        return_value=mocker.Mock(
            id=str(uuid4()),
            enc_data='test_enc_data',
        ),
    )

    response = client.patch(
        'api/v1/e-notes/update',
        json={'id': str(uuid4()), 'enc_data': 'test_enc_data'},
        headers=normal_user_token_headers,
    )

    assert response.status_code == 200
    assert mock_patch.call_count == 1


@pytest.mark.asyncio
async def test_delete_e_note_route(mocker: MockerFixture, client: TestClient, normal_user_token_headers):
    mock_delete = mocker.patch.object(DeleteENoteForUserUseCase, 'execute')

    e_note_id = str(uuid4())
    response = client.delete(f'api/v1/e-notes/{e_note_id}/delete', headers=normal_user_token_headers)

    assert response.status_code == 200
    assert mock_delete.call_count == 1
