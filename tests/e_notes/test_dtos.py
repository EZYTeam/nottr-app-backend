from uuid import uuid4

import pytest

from app.apps.e_notes.dtos import ENoteDTO
from app.core.config import get_settings
from app.lib.classes import BadRequestException

settings = get_settings()


@pytest.fixture(scope='module')
def test_e_note_dto_creation():
    e_note_id = uuid4()
    enc_data = 'test data'
    e_note = ENoteDTO(id=e_note_id, enc_data=enc_data)

    assert e_note.id == e_note_id
    assert e_note.enc_data == enc_data


@pytest.fixture(scope='module')
def test_e_note_dto_enc_data_validation():
    with pytest.raises(BadRequestException) as exc_info:
        too_big_enc_data = 'a' * (settings.MAX_E_NOTE_SIZE + 1)
        ENoteDTO(enc_data=too_big_enc_data)

    assert exc_info.value.detail == 'Max e_note size is reached.'
