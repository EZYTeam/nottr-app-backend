from typing import Any
from uuid import uuid4

import pytest
from pytest_mock import MockerFixture
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.apps.e_notes.dtos import ENoteDTO
from app.apps.e_notes.models import ENotes
from app.apps.e_notes.use_cases import (
    BaseENoteWithUserUseCase,
    CreateENoteForUserUseCase,
    DeleteENoteForUserUseCase,
    FetchENotesForUserUseCase,
    PatchENoteForUserUseCase,
)
from app.apps.users.models import Users
from app.core.db_config import AsyncSessionMaker
from app.lib.classes import BadRequestException

session_maker = AsyncSessionMaker


@pytest.fixture
async def create_e_note_and_user():
    async with session_maker() as session:
        user = Users(username='user')
        session.add(user)
        await session.flush()
        session.add(e_note := ENotes(user_id=user.id, enc_data='test'))
        await session.flush()
        await session.commit()

    return user, e_note


class BaseENoteWithUserUseCaseNonAbc(BaseENoteWithUserUseCase):
    def execute(self, session: AsyncSession) -> Any:
        pass


@pytest.mark.asyncio
async def test_get_e_note_for_user_by_id_successful(create_e_note_and_user):
    user, e_note = create_e_note_and_user

    async with session_maker() as session:
        e_note_dto = ENoteDTO(id=e_note.id, enc_data='test')
        use_case = BaseENoteWithUserUseCaseNonAbc(e_note_dto, user)
        result = await use_case._get_e_note_for_user_by_id(session)
        assert result.id == e_note.id
        assert result.user_id == user.id


@pytest.mark.asyncio
async def test_get_e_note_for_user_by_id_not_found():
    async with session_maker() as session:
        user = Users(username='test_user')
        session.add(user)
        await session.flush()

        e_note_dto = ENoteDTO(id=uuid4(), enc_data='test')
        use_case = BaseENoteWithUserUseCaseNonAbc(e_note_dto, user)
        with pytest.raises(BadRequestException) as exc_info:
            await use_case._get_e_note_for_user_by_id(session)

        assert exc_info.value.detail == 'ENote not found'


@pytest.mark.asyncio
async def test_fetch_e_notes_for_user_use_case():

    async with session_maker() as session:
        user = Users(username='test_user')
        session.add(user)
        await session.flush()
        created_data = []
        for data in ('test1', 'test2'):
            e_note = ENotes(user=user, enc_data=data)
            session.add(e_note)
            await session.flush()
            created_data.append((e_note.id, e_note.enc_data))
        await session.commit()

    fetch_e_notes_for_user = FetchENotesForUserUseCase(user)
    result = await fetch_e_notes_for_user.execute()
    assert result == [ENoteDTO(id=n_id, enc_data=data) for n_id, data in created_data]


@pytest.mark.asyncio
async def test_create_e_note_for_user_use_case_with_valid_data(create_e_note_and_user):
    user, _ = create_e_note_and_user

    create_e_note_use_case = CreateENoteForUserUseCase(user=user, e_note=ENoteDTO(enc_data='test'))
    created_e_note = await create_e_note_use_case.execute()

    assert created_e_note.id is not None
    assert created_e_note.enc_data == 'test'
    assert created_e_note.user_id == user.id


@pytest.mark.asyncio
async def test_create_e_note_for_user_use_case_with_already_existing_e_note(create_e_note_and_user):
    user, e_note = create_e_note_and_user

    create_e_note_use_case = CreateENoteForUserUseCase(
        user=user,
        e_note=ENoteDTO(
            id=e_note.id,
            enc_data='test',
        ),
    )
    with pytest.raises(BadRequestException) as exc_info:
        await create_e_note_use_case.execute()

    assert exc_info.value.detail == 'Can\'t create already existing ENote'


@pytest.mark.asyncio
async def test_patch_e_note_for_user_use_case(create_e_note_and_user):
    user, e_note = create_e_note_and_user

    updated_e_note = ENoteDTO(
        id=e_note.id,
        enc_data='some-updated-data',
    )

    use_case = PatchENoteForUserUseCase(user=user, e_note=updated_e_note)
    result = await use_case.execute()

    assert result.enc_data == updated_e_note.enc_data
    assert result.id == e_note.id


@pytest.mark.asyncio
async def test_patch_e_note_for_user_use_case_no_id(mocker: MockerFixture):
    e_note = ENoteDTO(
        enc_data='some-data',
    )

    use_case = PatchENoteForUserUseCase(user=mocker.MagicMock(), e_note=e_note)

    with pytest.raises(BadRequestException) as exception:
        await use_case.execute()

    assert str(exception.value.detail) == 'No e_note_id provided'


@pytest.mark.asyncio
async def test_patch_e_note_for_user_use_case_same_data(create_e_note_and_user):
    user, e_note = create_e_note_and_user

    use_case = PatchENoteForUserUseCase(user=user, e_note=ENoteDTO.from_orm(e_note))
    result = await use_case.execute()

    assert result.enc_data == e_note.enc_data
    assert result.id == e_note.id


@pytest.mark.asyncio
async def test_delete_e_note_for_user_success(create_e_note_and_user):
    user, e_note = create_e_note_and_user

    use_case = DeleteENoteForUserUseCase(e_note_id=e_note.id, user=user)
    result = await use_case.execute()

    assert result is None
    async with session_maker() as session:
        q = select(ENotes).where(ENotes.user_id == user.id, ENotes.id == e_note.id).limit(1)
        e_note: None | ENotes = (await session.scalars(q)).first()
        assert e_note is None


@pytest.mark.asyncio
async def test_delete_e_note_for_user_not_found(create_e_note_and_user):
    user, _ = create_e_note_and_user

    e_note_id = str(uuid4())
    use_case = DeleteENoteForUserUseCase(e_note_id=e_note_id, user=user)

    with pytest.raises(BadRequestException) as exc_info:
        await use_case.execute()
    assert exc_info.value.detail == 'ENote not found'
